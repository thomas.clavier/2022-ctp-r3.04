# TP Noté

## Description

On vous demande de faire un programme pour gérer un marché électronique :

- Le marché a une liste de membres qui ont un nom et une somme d’argent disponible (ex : Alice a 1 000 € et Bob a 1 500 €) ;
- Des produits sont mis en vente sur le marché, avec leur nom, leur prix, et leur vendeur (ex : Alice vend des « Actions Thermo-geek-o-stat » pour 150 €) ;
- Un acheteur peut acheter les produits en vente. Il faut vérifier que l’acheteur a assez d’argent disponible et penser à faire le transfert d’argent vers le vendeur. Si l’acheteur n’a pas les fonds, la vente ne se fait tout simplement pas ;
- La bourse garde la liste des produits en vente et déjà vendus (et à qui ils ont été vendus).

Du point de vue programmation, les contraintes sont les suivantes :

- Vous **devez** développer le programme en TDD ;
- Le code **doit** respecter les règles de Clean Code décrites plus loin ;
- Le programme **devra** être en MVC ;
- Remise sous forme d’un projet fork, **privé** de ce projet nommé de la façon suivante : `https://gitlab.univ-lille.fr/prenom.nom.etu/2022-ctp-r3.04` dans lequel vous inviterez votre enseignant de R3.04. (Attention au "namespace" : prenom.nom.etu)

Précisions sur l’implémentation :

- Pas de JavaFX, tout en ligne de commande ;
- Les tests se concentreront sur le modèle les sorties sur la console étant difficiles à tester ;
- Le MVC utilisera les classes Observer et Subject développées en TP ;
- Le Contrôleur (MVC) « génère » les évènements de vente et d’achat (nous appellerons ça un scénario, voir plus loin) ;
- Ces évènements sont « enregistrés » par le Modèle (i.e. la bourse) ;
- La Vue affiche dans la console l’état de la bourse et/ou des membres à chaque changement.

Vous pouvez créer des membres simplement au début d’un scénario (c-a-d contrôleur) :

- Alice, 1 000 € ;
- Bob, 4 000 € ;
- Charles, 800 € ;
- Denise, 700 €.

Vous devez proposer au moins un contrôleur (points bonus si plusieurs controleurs) :

- Scénario fixe avec des évènements séparés de 1 seconde chacun (v. `Thread` plus loin) :
    - temps 0 – création des membres ;
    - temps 1 – mise en vente de « Actions Thermo-geek-o-stat », prix 150 € par Alice ;
    - temps 2 – mise en vente de « IUT de Lille », prix 1 000 € par Bob ;
    - temps 3 – achat de l’offre 1 (les « Actions Thermo-geek-o-stat ») par Denise ;
    - temps 4 – mise en vente de « Paris-Roubaix », prix 350 €, par Charles.
- Scénario aléatoire : création toutes les secondes d’un évènement aléatoire (v. `Random` plus loin)
    - temps 0 – création des membres ;
    - Ventes créées aléatoirement
        - Nom généré aléatoirement par une combinaison de 1 qualificatif (« super », « extra », « big », « cheap ») et un mot (« truc », « machin », « crap »)
        - valeur choisie au hasard entre 100 € et 2 000 €
        - vendeur choisi au hasard parmi les membres.
    - Achat choisi aléatoirement entre les produits en vente et les acheteurs (le marché vérifie ensuite que l’achat est possible ou non).

Vous devez proposer au moins une « vue » (points bonus si plusieurs vues) :

- Affichage « complet » : tous les produits en vente ou vendus, situation de tous les membres ;
- Affichage « membres » : ne montre pas les produits en vente ou vendus, seulement la situation des membres ;
- Affichage « opérations » : montre les ventes et achats de produits pas la situation des membres.

## Notation

Le TP est assez long et avec beaucoup de contraintes. Vous ne pourrez peut-être pas tout faire. La notation ne se basera pas uniquement sur la réalisation des fonctionnalités, mais aussi sur le respect des consignes : qualité du code (Clean) et des tests (TDD). Plus de fonctionnalités (contrôleurs/vues optionnels) rapportent potentiellement plus de points, mais pas si c’est au détriment des autres consignes.

Utilisez scrupuleusement l’approche TDD pour garantir, pas à pas, que vous avez quelque chose de fonctionnel. Commencez simplement et avancez progressivement :

- Création d’un seul membre et affichage (en MVC) ;
- Création d’une opération vente ;
- Création d’une opération achat ;
- etc.

## Règles de Clean Code

Votre code devra respecter les règles suivantes :

- noms significatifs ;
- respect des conventions de nommage de Java (majuscules/minuscules) ;
- méthodes courtes (< 50 lignes) ;
- 4 paramètres au plus ;
- au plus 3 niveaux d’imbrication d’instructions (if/boucles/...) ;
- classes courtes (< 200 lignes) ;
- utilisation des visibilités appropriées ;
- attention à l’utilisation de static.

## Pause entre les évènements

Pour utiliser la méthode "sleep̀", les contrôleurs doivent :

- étendre Thread ;
- implémenter la méthode `run()` qui déroule le scénario
```Java
class UnScenario extends Thread {
  public void run() {
    ...
  }
}
```
- être créé normalement par un new sans paramètre et être démarré par l’appel à la méthode start() :
```Java
new UnScenario().start();
```
- la méthode `sleep( milliseconds)` attend le nombre de millisecondes spécifié en argument, donc `sleep(1000)` pour attendre 1 seconde.

## Génération aléatoire

Pour générer des évènements aléatoires :

- créez un générateur aléatoire
```Java
rand = new Random(System.currentTimeMillis());
```
- utilisez par exemple la méthode `nextInt(x)` qui génère un entier entre 0 (inclus) et x (exclus) :

choix d’un membre au hasard
```Java
int i = rand.nextInt( listeMembres.size() );
listeMembres.get(i);
```

création d’un prix au hasard entre 100 et 2000 de 100 en 100
```Java
(rand.nextInt(19) + 1) * 100
```
